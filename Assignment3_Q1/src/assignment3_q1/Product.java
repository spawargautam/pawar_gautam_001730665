/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment3_q1;

import java.util.ArrayList;

/**
 *
 * @author new
 */
public class Product {
     private ArrayList<Offering> OfferingList;
     

  public Product()
    {
        OfferingList=new ArrayList<>();
    }
  
    public ArrayList<Offering> getOfferingList() {
        return OfferingList;
    }

    public void setOfferingList(ArrayList<Offering> OfferingList) {
        this.OfferingList = OfferingList;
    }
    
    
    
    public Offering addOffering()
    {
        Offering o = new Offering();
        OfferingList.add(o);
        return o;
    }
    
    
   
    
}
