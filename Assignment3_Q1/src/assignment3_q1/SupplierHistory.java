/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment3_q1;

import java.util.ArrayList;

/**
 *
 * @author new
 */
public class SupplierHistory {
    
    
    private ArrayList<Supplier> SupplierList;

    public ArrayList<Supplier> getSupplierList() {
        return SupplierList;
    }

    public void setSupplierList(ArrayList<Supplier> SupplierList) {
        this.SupplierList = SupplierList;
    }
    
    public SupplierHistory()
            
    {
        SupplierList = new ArrayList<>();
    }
    
    public Supplier addSupplier()
    {
        Supplier s = new Supplier();
        SupplierList.add(s);
        return s;
    }
    
}
