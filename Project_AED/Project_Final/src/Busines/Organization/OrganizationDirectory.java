/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Busines.Organization;

import Busines.Organization.Organization.Type;
import java.util.ArrayList;

/**
 *
 * @author PAWAR
 */
public class OrganizationDirectory {
    
    private ArrayList<Organization> orgList;
    
    public OrganizationDirectory(){
        orgList = new ArrayList<>();
    }

    public ArrayList<Organization> getOrgList() {
        return orgList;
    }
    
    public Organization createOrganization(Type type)
    {
        Organization organization = null;
        if(type.getValue().equals(Type.Dietrician.getValue())){
            organization = new DietricianOrganization();
            orgList.add(organization);
          }
        
        else if(type.getValue().equals(Type.Trainer.getValue())){
            organization = new TrainerOrganization();
            orgList.add(organization);
           }
        else if(type.getValue().equals(Type.Hospital.getValue())){
            organization = new HospitalOrganization();
            orgList.add(organization);}
            
              else if(type.getValue().equals(Type.User.getValue())){
            organization = new UserOrganization();
            orgList.add(organization);}
        else if(type.getValue().equals(Type.Admin.getValue())){
            organization = new UserOrganization();
            orgList.add(organization);}
            
            
             
                      
              
        
        return organization;
}
}
