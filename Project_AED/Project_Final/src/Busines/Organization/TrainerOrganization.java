/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Busines.Organization;

import Business.Role.Role;
import Business.Role.Trainer_Role;
import java.util.ArrayList;

public class TrainerOrganization extends Organization{
    
    
    public TrainerOrganization()
    {
        super(Type.Trainer.getValue());
    }
            
     @Override
    public ArrayList<Role> roleList() 
    {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new Trainer_Role());
        return roles;
    }
}
