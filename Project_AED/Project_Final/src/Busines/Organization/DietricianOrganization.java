/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Busines.Organization;

import Business.Role.Dietrician_Role;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author PAWAR
 */
public class DietricianOrganization extends Organization{
    
    public DietricianOrganization()
    {
        super(Type.Dietrician.getValue());
    }
    
     @Override
    public ArrayList<Role> roleList() 
    {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new Dietrician_Role());
        return roles;
    }
}