/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Busines.Organization;

import Business.Role.Role;
import Business.Role.Trainer_Role;
import Business.Role.User_Role;
import java.util.ArrayList;

/**
 *
 * @author PAWAR
 */
public class UserOrganization extends Organization{
    public UserOrganization()
    {
        super(Type.User.getValue());
    }
            
     @Override
    public ArrayList<Role> roleList() 
    {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new User_Role());
        return roles;
    }
}
