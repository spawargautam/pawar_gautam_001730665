/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Busines.Organization;

import Business.Employee.EmployeeDirectory;
import Business.Role.Role;
import Business.User.User_Directory;
import Business.UserAccount.UserAccount;
import Business.UserAccount.UserAccountDirectory;
import Business.WorkQueue.WorkQueue;
import java.util.ArrayList;

/**
 *
 * @author PAWAR
 */
public abstract class Organization {
    
    private String name;
    private EmployeeDirectory employeeDirectory;
    private UserAccountDirectory userAccountDirectory;
    private User_Directory userDirectory;
    private WorkQueue workqueue;
    private int organizationID;
    private static int counter;
    
    
    public enum Type {
        Admin("Admin Organization"), 
        Trainer("Trainer Organization"),
        Dietrician("Dietrician Organization"),
        Hospital("Hospital Organization"),
        User("User Organization");
        private String value;
        private Type(String value) 
        {
            this.value = value;
        }
        public String getValue() 
        {
            return value;
        }
    }
    
    public Organization(String name)
    {
        this.name = name;
        workqueue = new WorkQueue();
        employeeDirectory = new EmployeeDirectory();
        userAccountDirectory = new UserAccountDirectory();
        userDirectory = new User_Directory();
        organizationID = counter;
        ++counter;


    }
    
        public abstract ArrayList<Role> roleList();

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }
        public UserAccountDirectory getUserAccountDirectory()
        {
            return userAccountDirectory;
        }

    public User_Directory getUserDirectory() {
        return userDirectory;
    }

      
    public String getorgName() 
    {
        return name;
    }

    public WorkQueue getWorkQueue() 
    {
        return workqueue;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public void setorgName(String name) 
    {
        this.name = name;
    }

    public void setWorkQueue(WorkQueue workQueue) 
    {
        this.workqueue = workQueue;
    }

    @Override
    public String toString() 
    {
        return name;
    }
    public boolean checkIfUsernameIsUnique(String username){
        for (UserAccount ua : userAccountDirectory.getUserAccountDirectory()){
            if (ua.getUsername().equals(username))
                return false;
        }
        return true;
    }
    
    }
    

