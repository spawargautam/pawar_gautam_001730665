/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Busines.Organization;

import Business.Role.Manager_Role;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author PAWAR
 */
public class HospitalOrganization extends Organization 
{
  public HospitalOrganization(){
      
      super(Type.Hospital.getValue());
  }    
   @Override
    public ArrayList<Role> roleList() 
    {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new Manager_Role());
        return roles;
    }
}
