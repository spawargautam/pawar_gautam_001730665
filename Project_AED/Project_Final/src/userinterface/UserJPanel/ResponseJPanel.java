

package userinterface.UserJPanel;

import Busines.Organization.UserOrganization;
import Business.UserAccount.UserAccount;
import Business.VitalSigns.VitalSigns;
import Business.WorkQueue.ReportRequest;
import Business.WorkQueue.TrainerWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.io.FileOutputStream;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
//import com.lowagie.text.pdf.PdfWriter;

import static com.sun.xml.internal.fastinfoset.alphabet.BuiltInRestrictedAlphabets.table;
import javax.swing.JTable;


public class ResponseJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private WorkRequest request;
    private UserOrganization userOrganization;
    private UserAccount userAccount;
    private TrainerWorkRequest trainerworkrequest;
    ReportRequest reportrequest;
    private VitalSigns vs;
     
    public ResponseJPanel(JPanel upc, WorkRequest request, UserAccount userAcoount,UserOrganization userOrganization) {
        initComponents();
            this.userProcessContainer = upc;
            this.request=request;
            this.userAccount=userAcoount;
            this.userOrganization=userOrganization;
            populateTable();
            
    }
         
        public void populateTable(){
        DefaultTableModel model = (DefaultTableModel)workRequestJTable.getModel();
        
        model.setRowCount(0);
        
        for(WorkRequest request :userAccount.getWorkQueue().getWorkRequestList()){
          if(request instanceof ReportRequest ){
             
            Object[] row = new Object[4];
       //     TrainerWorkRequest tq = (TrainerWorkRequest)request;
            row[0] = request;
            row[1] = request.getReceiver() == null ? null : request.getReceiver().getEmployee().getName();
            row[2] = request.getSender();
            row[3] = request.getStatus();
            
            model.addRow(row);
        }
    }       
     }
     public void populatedietTable()
     {
          int selectedRow = workRequestJTable.getSelectedRow();

          if (selectedRow < 0)
            return;
            
            
        DefaultTableModel model = (DefaultTableModel)dietTable.getModel();
        
        model.setRowCount(0);
        WorkRequest request = (WorkRequest)workRequestJTable.getValueAt(selectedRow, 0);
        for(VitalSigns vs: request.getUser().getVhs().getVitalList())
        {
              Object[] row = new Object[4];
               row[0] = vs.getMorningDiet();
               row[1] = vs.getNoonDiet();
               row[2] = vs.getEveningDiet();
               row[3] = vs.getNightDiet();
               //row[4]=  vs.getAge();
               //row[5]=  vs.getWeight();
               
               model.addRow(row); 
        }
     }
     @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        dietTable = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        workRequestJTable = new javax.swing.JTable();
        viewReportButton = new javax.swing.JButton();
        backButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        pdfButton = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(102, 102, 102));
        setForeground(new java.awt.Color(102, 102, 102));
        setLayout(null);

        jLabel1.setFont(new java.awt.Font("Trebuchet MS", 2, 18)); // NOI18N
        jLabel1.setText("Diet Report");
        add(jLabel1);
        jLabel1.setBounds(379, 639, 96, 21);

        dietTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Morning", "Noon", "Evening", "Night"
            }
        ));
        jScrollPane1.setViewportView(dietTable);

        add(jScrollPane1);
        jScrollPane1.setBounds(244, 678, 452, 175);

        workRequestJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Message", "Sender", "Receiver", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, true, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(workRequestJTable);

        add(jScrollPane2);
        jScrollPane2.setBounds(294, 24, 375, 96);

        viewReportButton.setText("View Report");
        viewReportButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewReportButtonActionPerformed(evt);
            }
        });
        add(viewReportButton);
        viewReportButton.setBounds(315, 576, 103, 25);

        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });
        add(backButton);
        backButton.setBounds(89, 906, 59, 25);

        jLabel2.setIcon(new javax.swing.ImageIcon("C:\\Users\\nikan\\Desktop\\diet.jpeg")); // NOI18N
        add(jLabel2);
        jLabel2.setBounds(210, 140, 630, 425);

        pdfButton.setText("Export to PDF");
        pdfButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pdfButtonActionPerformed(evt);
            }
        });
        add(pdfButton);
        pdfButton.setBounds(527, 576, 109, 25);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/userinterface/images/userhome.jpg"))); // NOI18N
        jLabel3.setText("jLabel3");
        add(jLabel3);
        jLabel3.setBounds(10, 10, 1180, 1000);
    }// </editor-fold>//GEN-END:initComponents

    private void viewReportButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewReportButtonActionPerformed
     populatedietTable();
    }//GEN-LAST:event_viewReportButtonActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backButtonActionPerformed

    private void pdfButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pdfButtonActionPerformed
    try{
    DefaultTableModel model = (DefaultTableModel)dietTable.getModel();    
    dietTable = new JTable(model);
   Document document=new Document();
   PdfWriter writer;
   
     writer=PdfWriter.getInstance(document,new FileOutputStream("d:/diet.pdf"));
     writer.setViewerPreferences(PdfWriter.PageLayoutSinglePage);

      
          document.open();
          PdfPTable tab = new PdfPTable(4);
          tab.addCell("Morning");
          tab.addCell("Noon");
          tab.addCell("Evening");
          tab.addCell("Night");
             int count=dietTable.getRowCount();
             
             for(int j = 0 ;j<count ; j++){
                 String morDiet = (String)dietTable.getValueAt(j,0);
                 String noonDiet = (String)dietTable.getValueAt(j,1);
                 String eveDiet = (String)dietTable.getValueAt(j,2);
                 String nightDiet = (String)dietTable.getValueAt(j,3);
                 System.out.println("Morning Diet:" +morDiet);
                 tab.addCell(morDiet);
                 tab.addCell(noonDiet);
                 tab.addCell(eveDiet);
                 tab.addCell(nightDiet);
             }
             document.add(tab);
                 
        
   document.close();
       }
       catch(Exception e){}
    
   // TODO add your handling code here:
    }//GEN-LAST:event_pdfButtonActionPerformed

    public Object GetData(JTable table, int row_index, int col_index){
    return table.getModel().getValueAt(row_index, col_index);
 }
 
    
     
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton;
    private javax.swing.JTable dietTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton pdfButton;
    private javax.swing.JButton viewReportButton;
    private javax.swing.JTable workRequestJTable;
    // End of variables declaration//GEN-END:variables
}
