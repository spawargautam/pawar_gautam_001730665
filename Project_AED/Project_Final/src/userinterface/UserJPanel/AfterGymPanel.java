/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.UserJPanel;

import Busines.Organization.Organization;
import Business.Enterprise.Enterprise;
import Business.User.User;
import Business.UserAccount.UserAccount;
import Business.VitalSigns.VitalSigns;
import Business.WorkQueue.HospitalWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class AfterGymPanel extends javax.swing.JPanel {

    JPanel userProcessContainer;
    Enterprise enterprise;
    private UserAccount userAccount;
    //private UserOrganization organization;
    private VitalSigns vitalsign;
    private WorkRequest request;
    private User user;

    public AfterGymPanel(JPanel upc, Enterprise enterprise, UserAccount account, WorkRequest request, VitalSigns vs) {
        initComponents();
        this.userProcessContainer = upc;
        this.enterprise = enterprise;
        this.userAccount = account;
        //   this.organization=organization;
        this.request = request;
        this.vitalsign = vs;
        populateFields();
        toField.setText("spawargautam@gmail.com");

//          for(Organization organ : enterprise.getOrganizationDirectory().getOrgList()){
//                    System.out.println(organ.getorgName());
//                    if(organ.getorgName().equals(Organization.Type.Hospital.getValue())) {
//                        float breatheRate = Float.parseFloat(breathRateField.getText());
//                        float heartrate = Float.parseFloat(heartRateField.getText());
//                        float bodytemp = Float.parseFloat(bpField.getText());
//                        if((breatheRate < 12 || breatheRate > 25)||(heartrate>60) || (bodytemp>45) )
//                        {
//                            
//                            HospitalWorkRequest hrequest = new HospitalWorkRequest();
//                            hrequest.setMessage2("Needs help");
//                            hrequest.setSender(userAccount);
//                            
//                            hrequest.setStatus("Sent");
//                            organ.getWorkQueue().getWorkRequestList().add(hrequest);
//                            userAccount.getWorkQueue().getWorkRequestList().add(hrequest);
//                            JOptionPane.showMessageDialog(null, "Emergency", "Warning", JOptionPane.PLAIN_MESSAGE);
//                        }
//                        
//                    }
//                }
//        populateTable();
        //System.out.println("this time"+vitalsign.getBodytemp());
    }

    public int mailSend(String recipient, String bodyContent) {
        String to = recipient;
        //String subject = subjectjTextField.getText();
        String messageToSender = toField.getText();
        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.socketFactory.port", "465");
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.port", "465");
        Session session = Session.getDefaultInstance(properties,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("donotreplyvirtualtrainer@gmail.com", "gautampawar");
                    }
                }
        );
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("donotreplydubs@gmail.com"));
            //   for(int i = 0; i<=to.length;i++) 
            String s = to;
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(s));

            message.setSubject("EmergencyHelp - Patient Needs Help");
            try {
             //   VitalSigns vitalsign = new VitalSigns();

                message.setText("Please Take Some Action and Send Some Help" + "Patient's" + userAccount.getUsername() + "" + "bloodpressure is" + bpField.getText() + ""
                        + "Breathing Rate is" + "" + "" + breathRateField.getText() + "body temperature is" + bodyTempField.getText() + "" + "" + "Heart Rate is" + heartRateField.getText());
                Transport.send(message);
                return 1;
            } catch (AddressException e) {
                JOptionPane.showMessageDialog(null, "It appears that your email address id not valid");
                return 2;
            }
            //message.setText(messageToSender);
            // Transport.send(message);
            // JOptionPane.showMessageDialog(null, "Message successfully sent");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return 2;
        }

    }

    public void populateFields() {

        for (VitalSigns vs : userAccount.getUser().getVhs().getVitalList()) {
            vs.updateVitalSigns(vs.getBreathRate(), vs.getHeartRate(), vs.getBodytemp(), vs.getBloodPressure(), vs.getAge());
            breathRateField.setText(String.valueOf(vs.getBreathRate()));
            heartRateField.setText(String.valueOf(vs.getHeartRate()));
            bodyTempField.setText(String.valueOf(vs.getBodytemp()));
            bpField.setText(String.valueOf(vs.getBloodPressure()));

        }
    }

//    public void populateTable()
//  {
//        DefaultTableModel model = (DefaultTableModel)userRequestTable.getModel();
//        
//        model.setRowCount(0);
//        
//        for (WorkRequest request : userAccount.getWorkQueue().getWorkRequestList()){
//            
//            
//            
//            
//            Object[] row = new Object[4];
//            row[0] = request;
//            row[1] = request.getReceiver();
//            row[2] = request.getStatus();
//            row[3] = request.getMessage();
//            model.addRow(row);
//            
//        }
//        
//  }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        breathRateField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        bodyTempField = new javax.swing.JTextField();
        heartRateField = new javax.swing.JTextField();
        bpField = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        backButton = new javax.swing.JButton();
        checkVital = new javax.swing.JButton();
        toField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(102, 102, 102));

        jLabel1.setText("Breath Rate");

        jLabel2.setText("Blood Pressure");

        jLabel3.setText("Heart Rate");

        jLabel4.setText("Body Temperature");

        jLabel5.setText("After Gym Vital Signs");

        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        checkVital.setText("Check");
        checkVital.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkVitalActionPerformed(evt);
            }
        });

        jLabel6.setText("Hospital's Mail Address");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(406, 406, 406)
                        .addComponent(jLabel5))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(296, 296, 296)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(bpField, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(bodyTempField, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(heartRateField, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(218, 218, 218)
                                .addComponent(breathRateField, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(461, 461, 461)
                        .addComponent(jLabel6))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(383, 383, 383)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(toField, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(backButton)
                                .addGap(171, 171, 171)
                                .addComponent(checkVital)))))
                .addContainerGap(339, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(jLabel5)
                .addGap(75, 75, 75)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(breathRateField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(45, 45, 45)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(heartRateField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(bodyTempField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(bpField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 71, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(backButton)
                    .addComponent(checkVital))
                .addGap(29, 29, 29)
                .addComponent(jLabel6)
                .addGap(50, 50, 50)
                .addComponent(toField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(204, 204, 204))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);        // TODO add your handling code here:
    }//GEN-LAST:event_backButtonActionPerformed

    private void checkVitalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkVitalActionPerformed
        for (Organization organ : enterprise.getOrganizationDirectory().getOrgList()) {
            System.out.println(organ.getorgName());
            if (organ.getorgName().equals(Organization.Type.Hospital.getValue())) 
            {
                float breatheRate = Float.parseFloat(breathRateField.getText());
                float heartrate = Float.parseFloat(heartRateField.getText());
                float bodytemp = Float.parseFloat(bpField.getText());

                if ((breatheRate < 12 || breatheRate > 25) || (heartrate <40 || heartrate>100) || (bodytemp > 45))
                {

                    HospitalWorkRequest hrequest = new HospitalWorkRequest();
                    hrequest.setMessage2("Needs help");
                    hrequest.setSender(userAccount);
                    hrequest.setStatus("Sent");
                    organ.getWorkQueue().getWorkRequestList().add(hrequest);
                    userAccount.getWorkQueue().getWorkRequestList().add(hrequest);
                    JOptionPane.showMessageDialog(this, "Emergency", "Warning", JOptionPane.PLAIN_MESSAGE);
                    JOptionPane.showMessageDialog(this, "Request Sent to Hospital", "Warning", JOptionPane.PLAIN_MESSAGE);

                    mailSend(toField.getText(), "Needs Help Please Provide Immediate Assistance");
                }
            else 
            {
                JOptionPane.showMessageDialog(null, "You're Good to Go", "Warning", JOptionPane.PLAIN_MESSAGE);
            }
            } 
        }
    }//GEN-LAST:event_checkVitalActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton;
    private javax.swing.JTextField bodyTempField;
    private javax.swing.JTextField bpField;
    private javax.swing.JTextField breathRateField;
    private javax.swing.JButton checkVital;
    private javax.swing.JTextField heartRateField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JTextField toField;
    // End of variables declaration//GEN-END:variables
}
