/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.HospitaWorkAreaJPanel;

import Busines.Organization.HospitalOrganization;
import Busines.Organization.Organization;
import Busines.Organization.UserOrganization;
import Business.Ecosystem;
import Business.Enterprise.Enterprise;
import Business.User.User;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.HospitalWorkRequest;
import Business.WorkQueue.WorkQueue;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author PAWAR
 */
public class HospitalWorkAreaJPanel extends javax.swing.JPanel {
    private JPanel userProcessContainer;
    private Ecosystem business;
    private UserAccount userAccount;
    private Organization org;
    public  HospitalOrganization hospitalOrganization; /// 29/11
    Enterprise enterprise;
    WorkRequest workRequest;
    public UserOrganization userorganization;
    private WorkQueue wq;
    private User user;
   
    /**
     * Creates new form HospitalWorkAreaJPanel
     */
    public HospitalWorkAreaJPanel(JPanel userProcessContainer, Enterprise enterprise, UserAccount account, HospitalOrganization hospitalOrganization, Ecosystem business) {
        initComponents();
        this.userProcessContainer=userProcessContainer;
        this.enterprise=enterprise;
        this.userAccount=account;
        this.hospitalOrganization=hospitalOrganization;
        this.business=business;
        populateTable();
    }
  public void populateTable(){
         
        DefaultTableModel model = (DefaultTableModel) HostpitalRequestTable.getModel();
        
        model.setRowCount(0);
        
            for(WorkRequest hospitalrequest : hospitalOrganization.getWorkQueue().getWorkRequestList()){
            Object[] row = new Object[4];
            HospitalWorkRequest hWork = (HospitalWorkRequest)hospitalrequest;
            row[0] = hospitalrequest;
            row[1] = hospitalrequest.getSender();
            row[2] = hospitalrequest.getReceiver() == null ? null : hospitalrequest .getReceiver().getEmployee().getName();
            row[3] = hospitalrequest.getStatus();
            
            model.addRow(row);
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        HostpitalRequestTable = new javax.swing.JTable();
        assignJButton = new javax.swing.JButton();
        processJButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(102, 102, 102));
        setLayout(null);

        HostpitalRequestTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Emergency Message", "Patient", "Health Official", "Status"
            }
        ));
        jScrollPane1.setViewportView(HostpitalRequestTable);

        add(jScrollPane1);
        jScrollPane1.setBounds(340, 590, 543, 160);

        assignJButton.setText("Accept Requests");
        assignJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignJButtonActionPerformed(evt);
            }
        });
        add(assignJButton);
        assignJButton.setBounds(190, 790, 127, 25);

        processJButton.setText("Process");
        processJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                processJButtonActionPerformed(evt);
            }
        });
        add(processJButton);
        processJButton.setBounds(830, 780, 77, 25);

        jLabel1.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 18)); // NOI18N
        jLabel1.setText("Virtual Medical Center");
        add(jLabel1);
        jLabel1.setBounds(430, 30, 242, 21);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/userinterface/images/medical_1.jpg"))); // NOI18N
        jLabel3.setText("jLabel3");
        add(jLabel3);
        jLabel3.setBounds(10, 10, 1040, 820);
    }// </editor-fold>//GEN-END:initComponents

    private void assignJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assignJButtonActionPerformed

        int selectedRow = HostpitalRequestTable.getSelectedRow();   

        if (selectedRow < 0)
        {
            return;
        }
    
        WorkRequest request = (WorkRequest)HostpitalRequestTable.getValueAt(selectedRow, 0);
        if((request.getStatus().equals("Sent")))
        {
        request.setReceiver(userAccount);
        request.setStatus("Processing");
        populateTable();
        }
        else if((request.getStatus().equals("Completed"))) 
        
        {
              JOptionPane.showMessageDialog(null, "Already Assigned", "Warning", JOptionPane.PLAIN_MESSAGE);

        }
        else if((request.getStatus().equals("Processing"))) 
        
        {
              JOptionPane.showMessageDialog(null, "Already Assigned", "Warning", JOptionPane.PLAIN_MESSAGE);

        }
        
    }//GEN-LAST:event_assignJButtonActionPerformed

    private void processJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_processJButtonActionPerformed

        int selectedRow = HostpitalRequestTable.getSelectedRow();

        if (selectedRow < 0){
            return;
        }

        HospitalWorkRequest request = (HospitalWorkRequest)HostpitalRequestTable.getValueAt(selectedRow, 0);

        request.setStatus("Processing");

        ProcessWorkRequestJPanel processWorkRequestJPanel = new ProcessWorkRequestJPanel(userProcessContainer, request);
        userProcessContainer.add("processWorkRequestJPanel", processWorkRequestJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);

    }//GEN-LAST:event_processJButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable HostpitalRequestTable;
    private javax.swing.JButton assignJButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton processJButton;
    // End of variables declaration//GEN-END:variables
}
