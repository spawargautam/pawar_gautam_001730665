/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.TrainerWorkAreaJPanel;

import Busines.Organization.Organization;
import Busines.Organization.UserOrganization;
import Business.Ecosystem;
import Business.Enterprise.Enterprise;
import Business.User.User;
import Business.UserAccount.UserAccount;
import Business.VitalSigns.VitalSigns;
import Business.WorkQueue.ReportRequest;
import Business.WorkQueue.TrainerWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import userinterface.SystemAdminWorkArea.SystemAdminWorkAreaJPanel;

/**
 *
 * @author PAWAR
 */
public class GenerateReportJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private Ecosystem business;
    private UserAccount userAccount;
    private Enterprise enterprise;
    private WorkRequest request;   
    private VitalSigns vitalsign;
    private User user;
    TrainerWorkRequest tRequest;
    private Organization organization;
    
    public GenerateReportJPanel(JPanel userProcessContainer,User user,WorkRequest request,UserAccount userAccount,Enterprise enterprise) {
        initComponents();
        this.userProcessContainer=userProcessContainer;
        this.user =user;
        this.request=request;
        this.enterprise=enterprise;
        this.userAccount=userAccount;
        populatefields();
    }
  
    
    public void populatefields(){
        for(VitalSigns vs: user.getVhs().getVitalList())
        {
             String r = vs.getRunningStatus(vs.getBreathRate(),vs.getHeartRate(),vs.getBodytemp(),vs.getBloodPressure(),vs.getWeight(),vs.getAge());
             vs.setRunning(r);
             
             String t = vs.getWorkoutType(vs.getBreathRate(),vs.getHeartRate(),vs.getBodytemp(),vs.getBloodPressure(),vs.getWeight(),vs.getAge());
             vs.setWorkoutype(t);
             
             
             String s = vs.getStrengthType(vs.getBreathRate(),vs.getHeartRate(),vs.getBodytemp(),vs.getBloodPressure(),vs.getWeight(),vs.getAge());
             vs.setStrengthType(s);
             
             String e = vs.getMachineExercises(vs.getBreathRate(), vs.getHeartRate(), vs.getBodytemp(),vs.getBloodPressure(),vs.getWeight(),vs.getAge());
             vs.setExerciseType(e);
             
             
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        reportUser = new javax.swing.JButton();
        reportField = new javax.swing.JTextField();
        backButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(102, 102, 102));
        setLayout(null);

        reportUser.setText("Send Training Report to User");
        reportUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reportUserActionPerformed(evt);
            }
        });
        add(reportUser);
        reportUser.setBounds(470, 190, 205, 25);

        reportField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reportFieldActionPerformed(evt);
            }
        });
        add(reportField);
        reportField.setBounds(500, 140, 141, 22);

        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });
        add(backButton);
        backButton.setBounds(330, 270, 59, 25);

        jLabel1.setText("Training Report ");
        add(jLabel1);
        jLabel1.setBounds(500, 50, 93, 16);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/userinterface/images/trainerarea.jpg"))); // NOI18N
        jLabel2.setText("jLabel2");
        add(jLabel2);
        jLabel2.setBounds(10, 10, 1200, 780);
    }// </editor-fold>//GEN-END:initComponents

    private void reportUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reportUserActionPerformed
 if((reportField.getText().length()==0))
        { 
            JOptionPane.showMessageDialog(this, "Message cannot be Empty!!"); 
        }
// else if((!reportField.getText().matches("[^a-z0-9]"))){
//                 JOptionPane.showMessageDialog(this, "Message cannot be characters!!"); 
//
// }
      else { 
     TrainerWorkRequest trequest = new TrainerWorkRequest();

     trequest.setUser(user);
     trequest.setStatus("Training generated");   
     trequest.setMessage3(reportField.getText());
     trequest.setSender(userAccount);

      Organization org=null;
      UserAccount userAccount = null;
         for(Organization organ : enterprise.getOrganizationDirectory().getOrgList()){
            if(organ instanceof UserOrganization){
                for(UserAccount ua : organ.getUserAccountDirectory().getUserAccountDirectory()){
                    if(ua.getUser().getUname().equals(user.getUname()))
                    userAccount = ua;

                }
            }    
        }
         trequest.setReceiver(userAccount);
        userAccount.getWorkQueue().getWorkRequestList().add(trequest);
        JOptionPane.showMessageDialog(this, "Training Report Sent to User"); 

 }
    }//GEN-LAST:event_reportUserActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);         // TODO add your handling code here:
    }//GEN-LAST:event_backButtonActionPerformed

    private void reportFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reportFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_reportFieldActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JTextField reportField;
    private javax.swing.JButton reportUser;
    // End of variables declaration//GEN-END:variables
}
