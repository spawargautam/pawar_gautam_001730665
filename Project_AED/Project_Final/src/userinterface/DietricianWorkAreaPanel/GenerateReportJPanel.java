/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.DietricianWorkAreaPanel;

import Busines.Organization.DietricianOrganization;
import Busines.Organization.Organization;
import Busines.Organization.TrainerOrganization;
import Busines.Organization.UserOrganization;
import Business.Ecosystem;
import Business.Enterprise.Enterprise;
import Business.User.User;
import Business.UserAccount.UserAccount;
import Business.VitalSigns.VitalSigns;
import Business.WorkQueue.TrainerWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import userinterface.SystemAdminWorkArea.SystemAdminWorkAreaJPanel;


public class GenerateReportJPanel extends  javax.swing.JPanel {

    private JPanel userProcessContainer;
    private Ecosystem business;
    private UserAccount userAccount;
    private DietricianOrganization dietOrganization;
    private Enterprise enterprise;
    private WorkRequest request;   
    private VitalSigns vitalSign;
    private User user;
    private Organization organization;
   // private VitalSigns vitalSign;
    
    
    public GenerateReportJPanel(JPanel userProcessContainer,User user,WorkRequest request,UserAccount userAccount,Enterprise enterprise,VitalSigns vitalSign) {
        initComponents();
        this.userProcessContainer=userProcessContainer;
        this.user =user;
        this.request=request;
        this.enterprise=enterprise;
        this.userAccount = userAccount;
        this.vitalSign=vitalSign;
        
        //vitalsign = new VitalSigns();
        populatefields();
        
        
    }
    
    
    public void populatefields()
    
    {
        
        System.out.println(user.getUname());   
        System.out.println(vitalSign.getBloodPressure());
        //for(VitalSigns vs: user.getVhs().getVitalList())
          
             String m = vitalSign.getMorningDiet(vitalSign.getBreathRate(),vitalSign.getHeartRate(),vitalSign.getBodytemp(),vitalSign.getBloodPressure(),vitalSign.getWeight(),vitalSign.getAge());
             morningField.setText(m);
             vitalSign.setMorningDiet(m);
             
             String n = vitalSign.getNoonDiet(vitalSign.getBreathRate(),vitalSign.getHeartRate(),vitalSign.getBodytemp(),vitalSign.getBloodPressure(),vitalSign.getWeight(),vitalSign.getAge());
             noonField.setText(n);
             vitalSign.setNoonDiet(n);
             
              String e = vitalSign.getEveningDiet(vitalSign.getBreathRate(),vitalSign.getHeartRate(),vitalSign.getBodytemp(),vitalSign.getBloodPressure(),vitalSign.getWeight(),vitalSign.getAge());
             eveningField.setText(e);
             vitalSign.setEveningDiet(e);
             
              String ni = vitalSign.getNightDiet(vitalSign.getBreathRate(),vitalSign.getHeartRate(),vitalSign.getBodytemp(),vitalSign.getBloodPressure(),vitalSign.getWeight(),vitalSign.getAge());
             nightField.setText(ni);
             vitalSign.setNightDiet(ni);
        
                 
    }
    
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        resultButton = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        morningField = new javax.swing.JTextField();
        noonField = new javax.swing.JTextField();
        eveningField = new javax.swing.JTextField();
        nightField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        messageField = new javax.swing.JTextField();
        sendButton = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(102, 102, 102));
        setLayout(null);

        resultButton.setText("Send to User");
        resultButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resultButtonActionPerformed(evt);
            }
        });
        add(resultButton);
        resultButton.setBounds(644, 342, 107, 25);

        jButton2.setText("Back");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        add(jButton2);
        jButton2.setBounds(370, 342, 59, 25);

        jLabel1.setText("Morning Diet");
        add(jLabel1);
        jLabel1.setBounds(374, 63, 72, 16);

        jLabel2.setText("Noon Diet");
        add(jLabel2);
        jLabel2.setBounds(374, 103, 55, 16);

        jLabel4.setText("Evening Diet");
        add(jLabel4);
        jLabel4.setBounds(374, 143, 70, 16);

        jLabel5.setText("Night Diet");
        add(jLabel5);
        jLabel5.setBounds(374, 183, 55, 16);
        add(morningField);
        morningField.setBounds(584, 60, 69, 22);
        add(noonField);
        noonField.setBounds(584, 100, 69, 22);
        add(eveningField);
        eveningField.setBounds(584, 140, 69, 22);
        add(nightField);
        nightField.setBounds(584, 180, 69, 22);

        jLabel3.setText("Message to Trainer");
        add(jLabel3);
        jLabel3.setBounds(362, 461, 111, 16);

        messageField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                messageFieldActionPerformed(evt);
            }
        });
        add(messageField);
        messageField.setBounds(644, 458, 119, 22);

        sendButton.setText("Send to Trainer ");
        sendButton.setEnabled(false);
        sendButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendButtonActionPerformed(evt);
            }
        });
        add(sendButton);
        sendButton.setBounds(469, 571, 127, 25);

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/userinterface/images/dietpanel_1.jpg"))); // NOI18N
        jLabel6.setText("jLabel6");
        add(jLabel6);
        jLabel6.setBounds(10, 20, 1150, 770);
    }// </editor-fold>//GEN-END:initComponents

    private void resultButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resultButtonActionPerformed
        request.setStatus("Done by Dietrician");
       // request.setSender(userAccount);
        request.setUser(user);
        sendButton.setEnabled(true);
       JOptionPane.showMessageDialog(this, "Diet Report Sent to User"); 

      //  request.setImage("C:\\Users\\nikan\\Desktop\\diet_plan");
    }//GEN-LAST:event_resultButtonActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);             
    }//GEN-LAST:event_jButton2ActionPerformed

    private void sendButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendButtonActionPerformed
      if((messageField.getText().length()==0))
        { 
            JOptionPane.showMessageDialog(this, "Message cannot be Empty!!"); 
        }
//         else if((!messageField.getText().matches("[^a-z0-9 ]"))){
//                 JOptionPane.showMessageDialog(this, "Message cannot be characters!!"); 
//
// }   
        
      else { 
        
        
        
        String message = messageField.getText();
        TrainerWorkRequest tRequest = new TrainerWorkRequest();
        
        tRequest.setMessage3(messageField.getText());
        tRequest.setUser(user);
        tRequest.setSender(userAccount);
        tRequest.setStatus("Sent");
       
        Organization org=null;
        System.out.println("Enterprise Name" +enterprise.getorgName());
        for(Organization organization : enterprise.getOrganizationDirectory().getOrgList()){
            if (organization instanceof TrainerOrganization)
            {
                System.out.println("Organization Name: " +organization.getorgName());
                org = organization;
            }
        }
        //Work Request for Dietrician himself, trainer Request
    //    userAccount.getWorkQueue().getWorkRequestList().add(tRequest);
        
        UserAccount userAccount_User=null;
        for(Organization organ : enterprise.getOrganizationDirectory().getOrgList()){
            if(organ instanceof UserOrganization){
                for(UserAccount ua : organ.getUserAccountDirectory().getUserAccountDirectory()){
                    if(ua.getUser().getUname().equals(user.getUname()))
                    userAccount_User = ua;
                }
            }    
        }
        //Work Request added to Trainer & User : Trainer Work Request
        org.getWorkQueue().getWorkRequestList().add(tRequest);
        JOptionPane.showMessageDialog(this, "Request Sent to Trainer"); 

        System.out.println("User Name " +userAccount_User.getUser().getUname());
      //  userAccount_User.getWorkQueue().getWorkRequestList().add(tRequest);//7-12
         
        userAccount_User.getWorkQueue().getWorkRequestList().add(request);
      }          
    }//GEN-LAST:event_sendButtonActionPerformed

    private void messageFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_messageFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_messageFieldActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField eveningField;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JTextField messageField;
    private javax.swing.JTextField morningField;
    private javax.swing.JTextField nightField;
    private javax.swing.JTextField noonField;
    private javax.swing.JButton resultButton;
    private javax.swing.JButton sendButton;
    // End of variables declaration//GEN-END:variables
}
