/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Busines.Organization.Organization;
import Busines.Organization.UserOrganization;
import Business.Ecosystem;
import Business.Enterprise.Enterprise;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;
import userinterface.UserJPanel.UserDetailsJPanel;
import userinterface.UserJPanel.userWelcomeJPanel;

/**
 *
 * @author PAWAR
 */
public class User_Role extends Role {
    
    
    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, Ecosystem business) {
        return new userWelcomeJPanel(userProcessContainer, enterprise, account, (UserOrganization)organization);
    }
    
}
