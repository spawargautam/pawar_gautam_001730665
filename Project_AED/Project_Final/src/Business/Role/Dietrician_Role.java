/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Busines.Organization.DietricianOrganization;
import Busines.Organization.Organization;
import Business.Ecosystem;
import Business.Enterprise.Enterprise;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;
import userinterface.DietricianWorkAreaPanel.DietricianWorkAreaJPanel;

/**
 *
 * @author PAWAR
 */
public class Dietrician_Role extends Role {
   
    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, Ecosystem business) {
        return new DietricianWorkAreaJPanel(userProcessContainer,enterprise, account, (DietricianOrganization)organization,business);
    }
       
    
}
