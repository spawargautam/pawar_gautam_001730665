/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Busines.Organization.Organization;
import Business.Ecosystem;
import Business.Enterprise.Enterprise;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;

/**
 *
 * @author PAWAR
 */
public abstract class Role {
    
    public enum RoleType{
        
        Admin("Admin"),
        Trainer("Trainer"),
        Dietrician("Diatrician"),
        Manager("Manager"),
        User("User");
         private String value;
         private RoleType(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
        
      }
        public abstract JPanel createWorkArea(JPanel userProcessContainer, 
            UserAccount account, 
            Organization organization, 
            Enterprise enterprise, 
            Ecosystem business);
    
         @Override
    public String toString() {
        return this.getClass().getName();
    }
}
