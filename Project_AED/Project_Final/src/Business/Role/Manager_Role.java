/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Busines.Organization.HospitalOrganization;
import Busines.Organization.Organization;
import Business.Ecosystem;
import Business.Enterprise.Enterprise;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;
import userinterface.HospitaWorkAreaJPanel.HospitalWorkAreaJPanel;

/**
 *
 * @author PAWAR
 */
public class Manager_Role extends Role{
    
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, Ecosystem business) {
        return new HospitalWorkAreaJPanel(userProcessContainer,enterprise, account, (HospitalOrganization)organization,business);
    }
}
