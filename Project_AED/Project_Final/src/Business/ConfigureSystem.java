/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Employee.Employee;
import Business.Role.SystemAdmin_Role;

import Business.UserAccount.UserAccount;


public class ConfigureSystem {
    public static Ecosystem configure(){
        
        Ecosystem system = Ecosystem.getInstance();
        Employee employee = system.getEmployeeDirectory().createEmployee("RRH");
        
        UserAccount ua = system.getUserAccountDirectory().createUserAccount("gautam", "gautam", employee, new SystemAdmin_Role());
        
        return system;
    }
    
}


