/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.User;

import Busines.Organization.Organization;
import Business.VitalSigns.VitalSignHistory;

/**
 *
 * @author PAWAR
 */
public class User {
    
    private String uname;
    private VitalSignHistory vhs;
    Organization organization;
        private int userID;
            private static int counter;



    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public VitalSignHistory getVhs() {
        return vhs;
    }

    public int getUserID() {
        return userID;
    }
   
    public User()
    {
       this.vhs= new VitalSignHistory();
        userID = counter;
        ++counter;

    }
    
    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    @Override
    public String toString() {
        return uname;
    }
    
    
}
