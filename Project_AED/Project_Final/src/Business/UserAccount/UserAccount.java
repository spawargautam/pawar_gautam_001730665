/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.UserAccount;

import static Busines.Organization.Organization.Type.User;
import Business.Employee.Employee;
import Business.Role.Role;
import Business.User.User;
import Business.VitalSigns.VitalSignHistory;
import Business.WorkQueue.WorkQueue;


public class UserAccount {
    
    private String username;
    private String password;
    private Employee employee;
    private Role role;
    private WorkQueue workQueue;
    private User user;
    
    private VitalSignHistory vhs1; 
    public UserAccount()
    {
        workQueue = new WorkQueue();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public VitalSignHistory getVhs1() {
        return vhs1;
    }

    public void setVhs1(VitalSignHistory vhs1) {
        this.vhs1 = vhs1;
    }
    
    @Override
    public String toString() {
        return username;
    }
      
}
