/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.VitalSigns;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

public class VitalSigns {
    
    private float breathRate;
    private float heartRate;
    private float bodytemp;
    private float bloodPressure;
    private float weight;
    private int age;
    private String morningDiet;
    private String noonDiet;
    private String eveningDiet;
    private String nightDiet;
    private String running;
    private String workoutype;
    private String strengthType;
    private String exerciseType;
//    Timer t = new Timer(7500, new ActionListener(){
//
//        @Override
//        public void actionPerformed(ActionEvent e) {
//            updateVitalSigns(breathRate,heartRate,bodytemp,bloodPressure,age);
//            System.out.println("Timer ON!!");
//        }
// });

    public String getExerciseType() {
        return exerciseType;
    }

    public void setExerciseType(String exerciseType) {
        this.exerciseType = exerciseType;
    }

    public String getStrengthType() {
        return strengthType;
    }

    public void setStrengthType(String strengthType) {
        this.strengthType = strengthType;
    }
    
    

    public float getWeight() {
        return weight;
    }

    public String getRunning() {
        return running;
    }

    public void setRunning(String running) {
        this.running = running;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getWorkoutype() {
        return workoutype;
    }

    public void setWorkoutype(String workoutype) {
        this.workoutype = workoutype;
    }
    

    public float getBreathRate() {
        return breathRate;
    }

    public void setBreathRate(float breathRate) {
//        t.start();
        
        this.breathRate = breathRate;
    }

    public float getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(float heartRate) {
        this.heartRate = heartRate;
    }

    public float getBodytemp() {
        return bodytemp;
    }

    public void setBodytemp(float bodyTemp) {
        this.bodytemp = bodyTemp;
    }

    public float getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(float bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public String getMorningDiet() {
        return morningDiet;
    }

    public void setMorningDiet(String morningDiet) {
        this.morningDiet = morningDiet;
    }

    public String getNoonDiet() {
        return noonDiet;
    }

    public void setNoonDiet(String noonDiet) {
        this.noonDiet = noonDiet;
    }

    public String getEveningDiet() {
        return eveningDiet;
    }

    public void setEveningDiet(String eveningDiet) {
        this.eveningDiet = eveningDiet;
    }

    public String getNightDiet() {
        return nightDiet;
    }

    public void setNightDiet(String nightDiet) {
        this.nightDiet = nightDiet;
    }

    @Override
    public String toString() {
        return String.valueOf(breathRate);
    }
    
    public String getMorningDiet(float breathRate,float heartRate,float bodytemp,float bloodPressure,float weight,int age)
    {
        if((breathRate>10 && breathRate<15) && (bodytemp>20 &&bodytemp<25)&& (heartRate>40 && heartRate<45) &&(bloodPressure>100 && bloodPressure<110) && (weight>50))
        {
         
        return("Bread Butter,Eggs,Milk,High Fiber Fruits");
        
        }
//       else if((breathRate>15 && breathRate <20) && (bodytemp>25 &&bodytemp<30) && (heartRate>45 && heartRate<50) &&((bloodPressure>110)&& bloodPressure<115 )&& weight>80 && (age>15))
//        {  
//         return("Low fat Dairy Products");    
//        }  
//       else if((breathRate>20 && breathRate <30) && (bodytemp>30 &&bodytemp<40) && (heartRate>50 && heartRate<60) &&((bloodPressure>115)&& bloodPressure<130)&& weight>80 && (age>15))
//        {  
//         return("Oil free food");    
//        }  
        else{
           return("Cereals,Nuts,Protein Suppliments"); 
    } }
    public String getNoonDiet(float breathRate,float heartRate,float bodyTemp,float bloodPressure,float weight,int age)
    {
        if((breathRate>10 && breathRate<15) && (bodytemp>20 &&bodytemp<25)&& (heartRate>40 && heartRate<45) &&(bloodPressure>100 && bloodPressure<110) && (weight>50))
        {
         
        return("Potato Vegetables, Roasted Chicken and Rice,");
        
        }
       else if((breathRate>15 && breathRate <20) && (bodytemp>25 &&bodytemp<30) && (heartRate>45 && heartRate<50) &&((bloodPressure>110)&& bloodPressure<115 )&& weight>80 && (age>15))
        {  
         return("Vegetables and Low fat Dairy Products");    
        }
       else if((breathRate>20 && breathRate <30) && (bodytemp>30 &&bodytemp<40) && (heartRate>50 && heartRate<60) &&((bloodPressure>115)&& bloodPressure<130)&& weight>80 && (age>15))
        {  
         return("Protein and Fiber Food");    
        }  
       else{
           return("Oatmeal and Walnuts"); 
        } }
    
    public String getEveningDiet(float breathRate,float heartRate,float bodytemp,float bloodPressure,float weight,int age)
    {
        if((breathRate>10 && breathRate<15) && (bodytemp>20 &&bodytemp<25)&& (heartRate>40 && heartRate<45) &&(bloodPressure>100 && bloodPressure<110) && (weight>50))
        {
         
        return(" Orange Juices and Tea");
        
        }
       else if((breathRate>15 && breathRate <20) && (bodytemp>25 &&bodytemp<30) && (heartRate>45 && heartRate<50) &&((bloodPressure>110)&& bloodPressure<115 )&& weight>80 && (age>15))
        {  
         return("Fiber Fruits and Fish salad");    
        }  
       else if((breathRate>20 && breathRate <30) && (bodytemp>30 &&bodytemp<40) && (heartRate>50 && heartRate<60) &&((bloodPressure>115)&& bloodPressure<130)&& weight>80 && (age>15))
        {  
         return("Vegetable Rice");    
        }  
       else{
           return("Cashews and Milk"); 
    } }
    
    public String getNightDiet(float breathRate,float heartRate,float bodytemp,float bloodPressure,float weight,int age)
    {
        if((breathRate>10 && breathRate<15) && (bodytemp>20 &&bodytemp<25)&& (heartRate>40 && heartRate<45) &&(bloodPressure>100 && bloodPressure<110) && (weight>50))
        {
         
        return("Salad and Milk");
        
        }
       else if((breathRate>15 && breathRate <20) && (bodytemp>25 &&bodytemp<30) && (heartRate>45 && heartRate<50) &&((bloodPressure>110)&& bloodPressure<115 )&& weight>80 && (age>15))
        {  
         return("Potato and Salty food");    
        }  
       else if((breathRate>20 && breathRate <30) && (bodytemp>30 &&bodytemp<40) && (heartRate>50 && heartRate<60) &&((bloodPressure>115)&& bloodPressure<130)&& weight>80 && (age>15))
        {  
         return("Chicken,Fish ");    
        }  
       else{
           return("Yogurt and Low fat dairy"); 
    } }
    
     public String getRunningStatus(float breathRate,float heartRate,float bodytemp,float bloodPressure, float weight, int age)
    {
        if((breathRate>12 && breathRate<18) && (heartRate>50 && heartRate<80) && ( (bloodPressure >90)&&(bloodPressure<120)) && weight>90 && age <40 ){
         
        return("High Speed Running");
        }
         else if((breathRate>18) && (heartRate<40) && ((bloodPressure>120))&& weight<50)
         {
             return("20 Mins Treadmill Walk");
         }
         else{ return("Average Speed Run");
    }}
     
   public String getWorkoutType(float breathRate,float heartRate,float bodyTemp,float bloodPressure,float weight,int age)
    {
        if((breathRate>12 && breathRate<18) && (heartRate>50 && heartRate<80) && ( (bloodPressure >90)&&(bloodPressure<120))&& age>25){
         
        return("Aerobics and Cycling");
        }
        else if((breathRate>18) && (heartRate>80) && ((bloodPressure>120)) && weight>60 && age>15)
        {  
        return("Yoga and Meditation");    
        } 
        else{
        return("Intensive Cross Fit Workout");
    }  }
    public String getStrengthType(float breathRate,float heartRate,float pulseRate,float bloodPressure,float weight,int age)
    {
        if(weight<50 && breathRate >12 && (bloodPressure>90 && bloodPressure<120)&& (heartRate>50 && heartRate<80) && age >18 ){
         
        return("Chest and Biceps Training");
        }
        else if(heartRate>80 && weight<50 && age>18 )
        {  
         return("Shoulder and Triceps");    
        }  
        else{ 
            return("Squat Speed Bag");
    }}
     public String getMachineExercises(float breathRate,float heartRate,float pulseRate,float bloodPressure,float weight,int age)
    {
        if(weight<50 && breathRate >12 && (bloodPressure>90 && bloodPressure<120)&& age>15 &&(heartRate>50 && heartRate<80)){
         
        return("Lateral Pull Down and Crunches");
        }
        else if(heartRate>80 && weight<50 && bloodPressure>80 && age>14 &&age<55)
        {  
         return("Butterfly and Sit ups");    
        }  
        
        else{
            
        
        return("Dumbbell Rack and Flat Bench");
    }  }
     
    
    public VitalSigns updateVitalSigns(float breathRate,float heartRate,float bodyTemp,float bloodPressure,int age)
     {
       
         this.age=age;
         this.breathRate= breathRate +15;
         this.heartRate= 220 - age; //Statistically speaking, the maximum heart rate is predictable based on a person’s age. Max heart rate decreases about one beat per year starting at roughly 10-15 years of age. To estimate maximum heart rate one would take the number 220 and subtract it from the person’s age. Note that this is an average and heart rate may fluctuate +/-12 BPM per individual.

         //Maximum Heart Rate Estimate: 220 – age
         this.bodytemp= bodyTemp + 20;
         this.bloodPressure= bloodPressure +10; // Absolute o Drop in SBP of greater than or equal to 10 mmHg from baseline BP despite an increase in workload when accompanied by other evidence of ischemia o Any decrease in SBP despite an increase in workload
         return this;
     }
     
}
