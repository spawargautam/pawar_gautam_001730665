/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.VitalSigns;

import Business.User.User;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author PAWAR
 */
public class VitalSignHistory {
  private User user;
    
    private ArrayList<VitalSigns> vitalList;
    
    
    public VitalSignHistory(){
        vitalList = new ArrayList<>();
    }

    public ArrayList<VitalSigns> getVitalList() {
        return vitalList;
    }
    
    public VitalSigns addVitalSign(){
        VitalSigns vs = new VitalSigns();
        vitalList.add(vs);
        return vs;
    }
    
    public void calculateStatus() {
        if (!user.getVhs().getVitalList().isEmpty()) {
            float r = 0, w = 0, b = 0, h = 0;
            float n = user.getVhs().getVitalList().size();
            for (VitalSigns vs : user.getVhs().getVitalList()) {
                r = r + vs.getBreathRate();
                w = w + vs.getHeartRate();
                b = b + vs.getBodytemp();
                h = h + vs.getBloodPressure();
            }
            float sumr = r / n;
            float sumw = w / n;
            float sumb = b / n;
            float sumh = h / n;
        }
    }
    
}
