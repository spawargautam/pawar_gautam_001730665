/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.User.User;


public class AdminWorkRequest extends WorkRequest
{
    
      private User user;

     private String amessage;
     private String response;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
     

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAmessage() {
        return amessage;
    }

    public void setAmessage(String amessage) {
        this.amessage = amessage;
    }


    @Override
    public String toString() {
        return amessage;
    }
    
    
    
    
    
}
