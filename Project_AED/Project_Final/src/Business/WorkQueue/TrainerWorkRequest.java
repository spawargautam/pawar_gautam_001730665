/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.User.User;

/**
 *
 * @author PAWAR
 */
public class TrainerWorkRequest extends WorkRequest{
    
    private User user;
   private String message3;
    
    public User getUser() {
        return user;
    }

    public String getMessage3() {
        return message3;
    }

    public void setMessage3(String message3) {
        this.message3 = message3;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return message3;
    }
    
    
}
