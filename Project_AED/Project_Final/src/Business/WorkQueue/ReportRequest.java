/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.VitalSigns.VitalSigns;

/**
 *
 * @author PAWAR
 */
public class ReportRequest extends WorkRequest {
    
   private VitalSigns vitalsign;

    public VitalSigns getVitalsign() {
        return vitalsign;
    }

    public void setVitalsign(VitalSigns vitalsign) {
        this.vitalsign = vitalsign;
    }
    
}
