/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Donor;

import Business.Employee.Employee;
import java.util.ArrayList;


public class Donor extends  Employee {
    private String bdType;
    private String Date;
    private int dID;
    private static int count = 1;
    private ArrayList<DonorSigns> donorSList;
    
  public Donor()
  {
    this.donorSList = new ArrayList<>();  
    this.dID = count;
    count += 1;
  }

    public int getDonorId() {
        return dID;
                
    }

    public String getBloodType() {
        return bdType;
    }

    public void setBloodType(String bloodType) {
        this.bdType = bloodType;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public DonorSigns addDonorSigns(){
        DonorSigns donorSign = new DonorSigns();
        this.donorSList.add(donorSign);
        return donorSign;
    }

    public ArrayList<DonorSigns> getDonorSignsList() {
        return donorSList;
    }

    public void setDonorSignsList(ArrayList<DonorSigns> donorSignsList) {
        this.donorSList = donorSignsList;
    }
    
    

    @Override
    public String toString() {
        return dID + "";
    }
    
}
