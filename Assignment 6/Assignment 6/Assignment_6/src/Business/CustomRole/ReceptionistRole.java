/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.CustomRole;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.Role.Role;
import Business.UserAccount.UserAccount;
import UserInterface.ReceptionistRole.ReceptionistWorkArea;
import javax.swing.JPanel;

/**
 *
 * @author Ankit
 */
public class ReceptionistRole extends Role {

    @Override
    public JPanel createWorkArea(JPanel panel, UserAccount ua, Organization o, Enterprise e, EcoSystem ecos) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    return new ReceptionistWorkArea(panel, ua, e,o);
    }
    
}
