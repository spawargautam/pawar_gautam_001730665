/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author new
 */
public class Patient {
    private String PatientName;
    private String PatientID;
    private int Age;
    private String PrimaryDoctorName;
    private String PreferredPharmacy;
    private VitalSignHistory vhs;
    
    public Patient(){
        this.vhs = new VitalSignHistory();
    }

    public VitalSignHistory getVhs() {
        return vhs;
    }

    public void setVhs(VitalSignHistory vhs) {
        this.vhs = vhs;
    }
    

    public String getPatientName() {
        return PatientName;
    }

    public void setPatientName(String PatientName) {
        this.PatientName = PatientName;
    }

    public String getPatientID() {
        return PatientID;
    }

    public void setPatientID(String PatientID) {
        this.PatientID = PatientID;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int Age) {
        this.Age = Age;
    }

    public String getPrimaryDoctorName() {
        return PrimaryDoctorName;
    }

    public void setPrimaryDoctorName(String PrimaryDoctorName) {
        this.PrimaryDoctorName = PrimaryDoctorName;
    }

    public String getPreferredPharmacy() {
        return PreferredPharmacy;
    }

    public void setPreferredPharmacy(String PreferredPharmacy) {
        this.PreferredPharmacy = PreferredPharmacy;
    }
}
