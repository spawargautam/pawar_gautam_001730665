/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import Business.Person;
import Business.Person_History;
import Business.VitalSign;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;


public class managePerson extends javax.swing.JPanel {
      
  private JPanel userProcessContainer;
  private Person_History personList;
  private Person person;
  
    
    
    public managePerson(JPanel upc, Person_History pl ) {
        initComponents();
        userProcessContainer = upc;
        personList = pl;
        populateTable();
        
    }
    private void populateTable(){
    DefaultTableModel dtm=(DefaultTableModel)personTable.getModel();
    int rowCount=personTable.getRowCount();
    for(int i = rowCount-1;i>=0;i--){
    dtm.removeRow(i);
    }
    
    for(Person p: personList.getPersonList())
    {
        Object row[] = new Object[3];
        row[0]=p;
        row[1]=p.getName();
        row[2]=p.getAge();
        dtm.addRow(row);
    }
    
    
    }
    public void refreshTable()
    {
        DefaultTableModel dtm = (DefaultTableModel) personTable.getModel();
        dtm.setRowCount(0);
        for(Person p : personList.getPersonList())
        {
            Object[] row = new Object[3];
            row[0]= p;
            row[1]=p.getName();
            row[2]=p.getAge();
            dtm.addRow(row);
        }
    }
       
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        personTable = new javax.swing.JTable();
        searchButton = new javax.swing.JButton();
        searchField = new javax.swing.JTextField();
        viewButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        backButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        vitalTable = new javax.swing.JTable();
        viewVital = new javax.swing.JButton();

        personTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Timestamp", "Person Name", "Person Age"
            }
        ));
        jScrollPane1.setViewportView(personTable);

        searchButton.setText("Search");
        searchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchButtonActionPerformed(evt);
            }
        });

        viewButton.setText("View");
        viewButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewButtonActionPerformed(evt);
            }
        });

        deleteButton.setText("Delete");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });

        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        vitalTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Respiratory Rate", "Heart Rate", "Blood Pressure", "Weight"
            }
        ));
        jScrollPane2.setViewportView(vitalTable);

        viewVital.setText("View Vital Sign");
        viewVital.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewVitalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(77, 77, 77)
                        .addComponent(backButton)
                        .addGap(116, 116, 116)
                        .addComponent(viewButton)
                        .addGap(81, 81, 81)
                        .addComponent(deleteButton))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(135, 135, 135)
                        .addComponent(searchButton)
                        .addGap(120, 120, 120)
                        .addComponent(searchField, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(211, 211, 211)
                        .addComponent(viewVital)))
                .addContainerGap(151, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 284, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(viewVital)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 48, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(searchField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchButton, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(viewButton)
                    .addComponent(deleteButton)
                    .addComponent(backButton))
                .addGap(41, 41, 41))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void searchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchButtonActionPerformed
        
       String searchName = searchField.getText();
       Person p =   personList.searchPerson(searchName);
          if(p == null){
              JOptionPane.showMessageDialog(this,"Person not found", "Error", JOptionPane.ERROR_MESSAGE);
              return;
          }
          else{
              ViewPersonPanel vp = new ViewPersonPanel(userProcessContainer,p);
              userProcessContainer.add("View Panel",vp);
              CardLayout layout =(CardLayout)userProcessContainer.getLayout();
              layout.next(userProcessContainer);
          }
          
    }//GEN-LAST:event_searchButtonActionPerformed

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
       int selectedRow = personTable.getSelectedRow();
        if(selectedRow>=0){
            int dialogButton=JOptionPane.YES_NO_OPTION;
            int dialogResult= JOptionPane.showConfirmDialog(this,"Are you sure you want to delete?","Warning",dialogButton);
            if(dialogResult==JOptionPane.YES_OPTION){
        Person p=(Person)personTable.getValueAt(selectedRow,0);
        personList.deletePerson(p);
        populateTable();
        
        
        
        }
        else{
            JOptionPane.showMessageDialog(this,"Please select a row","Warning",JOptionPane.WARNING_MESSAGE);
        }
        
        }
    
    }//GEN-LAST:event_deleteButtonActionPerformed
    
    private void viewButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewButtonActionPerformed
        
        
        int selectedRow = personTable.getSelectedRow();
        if (selectedRow < 0) 
        {
        JOptionPane.showMessageDialog(null, "Please select a row from the table first to view detail", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        else
        {
                Person person = (Person)personTable.getValueAt(selectedRow, 0);
                ViewPersonPanel vp1 = new ViewPersonPanel(userProcessContainer, person);
                userProcessContainer.add("ViewPersonPanel", vp1);
                CardLayout layout = (CardLayout) userProcessContainer.getLayout();
                layout.next(userProcessContainer);
        }   
    }//GEN-LAST:event_viewButtonActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout =(CardLayout)userProcessContainer.getLayout();
        layout.previous(userProcessContainer); 
    }//GEN-LAST:event_backButtonActionPerformed

    private void viewVitalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewVitalActionPerformed

        int selectedRow = personTable.getSelectedRow();
        if(selectedRow>=0)
        {
            DefaultTableModel dtm = (DefaultTableModel)vitalTable.getModel();
            dtm.setRowCount(0);
            Person p = (Person)personTable.getValueAt(selectedRow, 0);
            for(VitalSign vs : p.getVhs().getVitalSignList())
            {
                Object row[]=new Object[5];
                row[0]=vs;
                row[1]=vs.getHeartRate();
                row[2]=vs.getSystolicbloodPressure();
                row[3]=vs.getWeightinPounds();
                
                        String status = showStatus(vs);
                        row[4]= status;
            dtm.addRow(row);
            }
        }
        else 
        {
            JOptionPane.showMessageDialog(this,"Please select a row","Warning",JOptionPane.WARNING_MESSAGE);
        }
    }                                             

                                          

private String showStatus(VitalSign vs){
    int selectedRow = personTable.getSelectedRow();
    Person person = (Person)personTable.getValueAt(selectedRow, 0);
        String status = "Abnormal";
        if(person.getAge()>=1 && person.getAge()<=3) 
        {
            if((vs.getRespiratoryRate()>=20 && vs.getRespiratoryRate()<=30) 
                    && (vs.getHeartRate()>=80 && vs.getHeartRate()<=130) 
                    && (vs.getSystolicbloodPressure()>=80 && vs.getSystolicbloodPressure()<=110) 
                    && (vs.getWeightinPounds()>=22 && vs.getWeightinPounds()<=31))
            {
                status = "Normal";
            }
        
        }         
         else   if(person.getAge()>=4 && person.getAge()<=5)
             {
             if((vs.getRespiratoryRate()>=20 && vs.getRespiratoryRate()<=30) 
                     && (vs.getHeartRate()>=80 && vs.getHeartRate()<=120)
                     && (vs.getSystolicbloodPressure()>=80 && vs.getSystolicbloodPressure()<=110) 
                     && (vs.getWeightinPounds()>=31 && vs.getWeightinPounds()<=40))
             { 
                 status = "Normal";
             }
             }
        
        
         else if(person.getAge()>=6 && person.getAge()<=12)
             {
             if((vs.getRespiratoryRate()>=20 && vs.getRespiratoryRate()<=30)
                 && (vs.getHeartRate()>=70 && vs.getHeartRate()<=110) 
                 &&  (vs.getSystolicbloodPressure()>=80 && vs.getSystolicbloodPressure()<=120)
                 && (vs.getWeightinPounds()>=41 && vs.getWeightinPounds()<=92))
             { 
                 status = "Normal";
             } 
             }
         
            
         else if(person.getAge()>=13)
         {
             if((vs.getRespiratoryRate()>=12 && vs.getRespiratoryRate()<=20)
                &&  (vs.getHeartRate()>=55 && vs.getHeartRate()<=105) 
                &&  (vs.getSystolicbloodPressure()>=110 && vs.getSystolicbloodPressure()<=120)
                &&  (vs.getWeightinPounds()>110))
             { 
                 status = "Normal";
             }
         
         }
    
            
   
 return status;
 
        
        
        
        
        
        
        
        
        
        
        
        
        
    }//GEN-LAST:event_viewVitalActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton;
    private javax.swing.JButton deleteButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable personTable;
    private javax.swing.JButton searchButton;
    private javax.swing.JTextField searchField;
    private javax.swing.JButton viewButton;
    private javax.swing.JButton viewVital;
    private javax.swing.JTable vitalTable;
    // End of variables declaration//GEN-END:variables
}
