/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import Business.Person_History;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author new
 */
public class HomePanel extends javax.swing.JPanel {
    
    private JPanel userProcessContainer;
    private Person_History personHistory;

    /**
     * Creates new form HomePanel
     */
    public HomePanel(JPanel userProcessContainer, Person_History personHistory)
    {
        initComponents();
          this.userProcessContainer = userProcessContainer;
          this.personHistory = personHistory;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        personProfileJButton = new javax.swing.JButton();
        patientProfileJbutton = new javax.swing.JButton();

        setLayout(new java.awt.BorderLayout());

        personProfileJButton.setText("Person Profile");
        personProfileJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                personProfileJButtonActionPerformed(evt);
            }
        });

        patientProfileJbutton.setText("Patient Profile");
        patientProfileJbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patientProfileJbuttonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(239, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(patientProfileJbutton)
                    .addComponent(personProfileJButton))
                .addGap(252, 252, 252))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addComponent(personProfileJButton)
                .addGap(86, 86, 86)
                .addComponent(patientProfileJbutton)
                .addContainerGap(232, Short.MAX_VALUE))
        );

        add(jPanel1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void patientProfileJbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_patientProfileJbuttonActionPerformed
        CreateJPanel1 cp1 = new CreateJPanel1(userProcessContainer,personHistory);
        userProcessContainer.add("HomeArea",cp1);
        CardLayout layout = (CardLayout)userProcessContainer.getLayout(); // using  card layout as we are using same panel to perform different actions like update,manage on the same panel.
        layout.next(userProcessContainer);
        
     
    }//GEN-LAST:event_patientProfileJbuttonActionPerformed

    private void personProfileJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_personProfileJButtonActionPerformed
        // TODO add your handling code here:
        CreateJPanel cp = new CreateJPanel(userProcessContainer,personHistory);
        userProcessContainer.add("HomeArea",cp);
        CardLayout layout = (CardLayout)userProcessContainer.getLayout(); // using  card layout as we are using same panel to perform different actions like update,manage on the same panel.
        layout.next(userProcessContainer);
   
    }//GEN-LAST:event_personProfileJButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton patientProfileJbutton;
    private javax.swing.JButton personProfileJButton;
    // End of variables declaration//GEN-END:variables
}
