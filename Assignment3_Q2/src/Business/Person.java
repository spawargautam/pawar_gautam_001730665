/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author new
 */
public class Person 
{

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int Age) {
        this.Age = Age;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getTown() {
        return Town;
    }

    public void setTown(String Town) {
        this.Town = Town;
    }
    
    private String Name;
    private int Age;
    private String Address;
    private String Town;
    private VitalSignHistory vhs;
    
    public Person(){
        this.vhs = new VitalSignHistory();
    }

    public VitalSignHistory getVhs() {
        return vhs;
    }

    public void setVhs(VitalSignHistory vhs) {
        this.vhs = vhs;
    }

    
    
    
    
    
    
    
    
    
}
