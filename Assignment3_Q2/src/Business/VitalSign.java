/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author new
 */
public class VitalSign {
    
   
    private int RespiratoryRate;
    private int HeartRate;
    private int SystolicbloodPressure;
    private float WeightinPounds;
    private int Timestamp;
    java.util.Date date = new java.util.Date();

   
    
    public int getRespiratoryRate() {
        return RespiratoryRate;
    }

    public void setRespiratoryRate(int RespiratoryRate) {
        this.RespiratoryRate = RespiratoryRate;
    }

    public int getHeartRate() {
        return HeartRate;
    }

    public void setHeartRate(int HeartRate) {
        this.HeartRate = HeartRate;
    }

    public int getSystolicbloodPressure() {
        return SystolicbloodPressure;
    }

    public void setSystolicbloodPressure(int SystolicbloodPressure) {
        this.SystolicbloodPressure = SystolicbloodPressure;
    }

    public float getWeightinPounds() {
        return WeightinPounds;
    }

    public void setWeightinPounds(float WeightinPounds) {
        this.WeightinPounds = WeightinPounds;
    }

    public int getTimestamp() {
        return Timestamp;
    }

    @Override
    public String toString() {
        return "" + date;
    }

    public void setTimestamp(int Timestamp) {
        this.Timestamp = Timestamp;
    }
    
}
