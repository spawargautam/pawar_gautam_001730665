/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;



/**
 *
 * @author new
 */
public class Person_History {
    
  private ArrayList<Person> personList;

  public Person_History(){
      personList = new ArrayList<>();
  }  
  public ArrayList<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(ArrayList<Person> personList) {
        this.personList = personList;
    }
  
  public Person addPerson()
  {
   Person person = new Person();
   personList.add(person);
   return person;
   
  }
  
  public void deletePerson(Person person)
  {
      personList.remove(person);
      
  }
  
  public Person searchPerson(String name)
  {
      for(Person p :this.personList){
        if(p.getName().equals(name))
            return p;
            
      }
      return null;
  }
  
  
}
