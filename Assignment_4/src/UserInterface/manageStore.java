/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import Business.Drugs;
import Business.Manufacturer;
import Business.Store;
import Business.manufacturerDirectory;
import Business.storeDirectory;
import java.awt.CardLayout;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author new
 */
public class manageStore extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private storeDirectory storedirectory;
    private manufacturerDirectory mdirectory;
    
    
    
    public manageStore(JPanel userProcessContainer,storeDirectory storedirectory,manufacturerDirectory mdirectory) {
        initComponents();
        this.userProcessContainer=userProcessContainer;
        this.storedirectory=storedirectory;
        this.mdirectory=mdirectory;
        populateTable();

        
    }
    
    private void populateTable(){
    DefaultTableModel dtm=(DefaultTableModel)storeTable.getModel();
    int rowCount=storeTable.getRowCount();
    for(int i = rowCount-1;i>=0;i--){
    dtm.removeRow(i);
    }
    
    for(Store s: storedirectory.getStoreList())
    {
        Object row[] = new Object[2];
        row[0]=s;
        row[1]=s.getStoreId();
        dtm.addRow(row);
    }
     }
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        storeTable = new javax.swing.JTable();
        viewDrugButton = new javax.swing.JButton();
        backButton = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        drugTable = new javax.swing.JTable();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable1);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(jTable2);

        setLayout(new java.awt.BorderLayout());

        storeTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Store Name", "Store ID"
            }
        ));
        jScrollPane1.setViewportView(storeTable);

        viewDrugButton.setText("View Drugs");
        viewDrugButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewDrugButtonActionPerformed(evt);
            }
        });

        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        drugTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "DrugName", "Manufacturers Name", "Price", "Quantity"
            }
        ));
        jScrollPane4.setViewportView(drugTable);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(106, 106, 106)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(176, 176, 176)
                        .addComponent(backButton)
                        .addGap(205, 205, 205)
                        .addComponent(viewDrugButton)))
                .addContainerGap(128, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(viewDrugButton)
                    .addComponent(backButton))
                .addContainerGap(333, Short.MAX_VALUE))
        );

        add(jPanel1, java.awt.BorderLayout.PAGE_START);
    }// </editor-fold>//GEN-END:initComponents

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
    userProcessContainer.remove(this);
    CardLayout layout =(CardLayout)userProcessContainer.getLayout();
    layout.previous(userProcessContainer); 


    }//GEN-LAST:event_backButtonActionPerformed

    private void viewDrugButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewDrugButtonActionPerformed
        int selectedRow = storeTable.getSelectedRow();
        if(selectedRow>=0)
        {
            DefaultTableModel dtm = (DefaultTableModel)drugTable.getModel();
            dtm.setRowCount(0);
            Store s = (Store)storeTable.getValueAt(selectedRow, 0);
            for(Drugs drug : s.getManufacturer().getDrugcatalog().getDrugList())
            {
                Object row[]=new Object[4];
                row[0]=  drug;
                row[1]=  drug.getManufacturer();
                row[2] = drug.getSuggested_Price();
                row[3] = drug.getQuantity();
                        
            dtm.addRow(row);
            }
        }
        else 
        {
            JOptionPane.showMessageDialog(this,"Please select a row","Warning",JOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_viewDrugButtonActionPerformed

    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton;
    private javax.swing.JTable drugTable;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable storeTable;
    private javax.swing.JButton viewDrugButton;
    // End of variables declaration//GEN-END:variables
}
