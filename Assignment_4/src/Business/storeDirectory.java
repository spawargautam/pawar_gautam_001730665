/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author new
 */
public class storeDirectory {
    
    private ArrayList<Store> storeList;

    public ArrayList<Store> getStoreList() {
        return storeList;
    }

    public void setStoreList(ArrayList<Store> storeList) {
        this.storeList = storeList;
    }
    
    public storeDirectory()
    {
        this.storeList= new ArrayList<Store>();
    }
    
    public Store addStore()
    {
        Store store = new Store();
        storeList.add(store);
        return store;
    }
    
    
}
