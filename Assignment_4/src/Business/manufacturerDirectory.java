/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Manufacturer;
import java.util.ArrayList;

/**
 *
 * @author new
 */
public class manufacturerDirectory {
    
    private ArrayList<Manufacturer> manufacturerList;

    public ArrayList<Manufacturer> getManufacturerList() {
        return manufacturerList;
    }

    public void setManufacturerList(ArrayList<Manufacturer> manufacturerList) {
        this.manufacturerList = manufacturerList;
    }
    
    
    public manufacturerDirectory()
    {
        manufacturerList = new ArrayList<Manufacturer>();
    }
    
    
    public Manufacturer addManufacturer()
    {
        Manufacturer m = new Manufacturer();
        manufacturerList.add(m);
        return m;
    }
    
}
