/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author new
 */
public class Manufacturer {
    
    
    private String manufacturerName;
    private drugCatalog drugcatalog;

    
    
    

    public String getManufacturerName() {
        return manufacturerName;
    }

    @Override
    public String toString() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }
    
    public Manufacturer()
    {
        this.drugcatalog=new drugCatalog();
    
    }

    public drugCatalog getDrugcatalog() {
        return drugcatalog;
    }

    public void setDrugcatalog(drugCatalog drugcatalog) {
        this.drugcatalog = drugcatalog;
    }
    
    
}
