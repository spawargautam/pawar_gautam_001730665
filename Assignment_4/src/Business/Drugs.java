/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author new
 */
public class Drugs {
    
    
    private String Name;
    private String manufacturer;
    private float suggested_Price;
    private int Quantity;
    private String expDate;
    private String drugType;
    private String drugStability;
    private String drugEffect;
    private String drugRating;
    private String dosageForm;
    private drugCatalog drugcatalog; 

    public drugCatalog getDrugcatalog() {
        return drugcatalog;
    }

    public void setDrugcatalog(drugCatalog drugcatalog) {
        this.drugcatalog = drugcatalog;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public float getSuggested_Price() {
        return suggested_Price;
    }

    public void setSuggested_Price(float suggested_Price) {
        this.suggested_Price = suggested_Price;
    }

   

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int Quantity) {
        this.Quantity = Quantity;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    @Override
    public String toString() {
        return Name;
    }

    public String getDrugType() {
        return drugType;
    }

    public void setDrugType(String drugType) {
        this.drugType = drugType;
    }

    public String getDrugStability() {
        return drugStability;
    }

    public void setDrugStability(String drugStability) {
        this.drugStability = drugStability;
    }

    public String getDrugEffect() {
        return drugEffect;
    }

    public void setDrugEffect(String drugEffect) {
        this.drugEffect = drugEffect;
    }

    public String getDrugRating() {
        return drugRating;
    }

    public void setDrugRating(String drugRating) {
        this.drugRating = drugRating;
    }

    public String getDosageForm() {
        return dosageForm;
    }

    public void setDosageForm(String dosageForm) {
        this.dosageForm = dosageForm;
    }
    
    public Drugs()
    {
        this.drugcatalog= new drugCatalog();
    }
    
}
