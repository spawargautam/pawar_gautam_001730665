/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author new
 */
public class Store {
    


private String storeName;
private String storeId;
private String storeLocation;
private String storeCity;
private storeDirectory vhs;
private drugCatalog dhs;
private Manufacturer manufacturer;

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public drugCatalog getDhs() {
        return dhs;
    }

    public void setDhs(drugCatalog dhs) {
        this.dhs = dhs;
    }

    @Override
    public String toString() {
        return storeName;
    }

    public storeDirectory getVhs() {
        return vhs;
    }

    public void setVhs(storeDirectory vhs) {
        this.vhs = vhs;
    }
    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreLocation() {
        return storeLocation;
    }

    public void setStoreLocation(String storeLocation) {
        this.storeLocation = storeLocation;
    }

    public String getStoreCity() {
        return storeCity;
    }

    public void setStoreCity(String storeCity) {
        this.storeCity = storeCity;
    }
    public Store()
    {
        this.vhs = new storeDirectory();
        this.dhs = new drugCatalog();
        manufacturer = new Manufacturer();
    }
    
}
