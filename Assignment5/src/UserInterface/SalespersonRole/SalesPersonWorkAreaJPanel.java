/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.SalespersonRole;

import Business.Product;
import Business.ProductCatalog;
import Business.salesPerson;
import Business.salespersonDirectory;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author new
 */
public class SalesPersonWorkAreaJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private salespersonDirectory salesdirectory;
    private ProductCatalog productcatalog;
    private salesPerson salesperson;
    private Product product;
    public SalesPersonWorkAreaJPanel(JPanel upc,ProductCatalog productcatalog,salespersonDirectory sd,salesPerson sp) {
        initComponents();
        this.userProcessContainer = upc;
        this.salesdirectory=sd;
        this.productcatalog=productcatalog;
        this.salesperson=sp;
        populateTable();
    }
    
    
    private void populateTable(){
    DefaultTableModel dtm=(DefaultTableModel)productCatalog.getModel();
    int rowCount=productCatalog.getRowCount();
    for(int i = rowCount-1;i>=0;i--){
    dtm.removeRow(i);
    }
    
    for(Product p: productcatalog.getProductcatalog())
    {
        Object row[] = new Object[5];
        row[0]=p;
        row[1]=p.getFloorprice();
        row[2]=p.getCeilingprice();
        row[3]=p.getTargetprice();
        row[4]=p.getAvail();
        dtm.addRow(row);
    }
     }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        productCatalog = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        addProductButton = new javax.swing.JButton();
        backButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        commisionField = new javax.swing.JTextField();

        productCatalog.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        productCatalog.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Product Name", "Floor Price", "Ceiling Price", "Target Price", "Availability"
            }
        ));
        jScrollPane1.setViewportView(productCatalog);

        jLabel1.setText("Product Catalog");

        addProductButton.setText("Add Products");
        addProductButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addProductButtonActionPerformed(evt);
            }
        });

        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        jLabel2.setText("Commision");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(backButton)
                                .addGap(112, 112, 112)
                                .addComponent(addProductButton)
                                .addGap(151, 151, 151)
                                .addComponent(jLabel2)
                                .addGap(27, 27, 27)
                                .addComponent(commisionField, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(138, 138, 138)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 401, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(88, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel1)
                .addGap(36, 36, 36)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(53, 53, 53)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addProductButton)
                    .addComponent(backButton)
                    .addComponent(jLabel2)
                    .addComponent(commisionField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(158, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void addProductButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addProductButtonActionPerformed
      int count = 0;     
        int selectedRow[] = productCatalog.getSelectedRows();
        System.out.println("Length of selected rows:"  +selectedRow.length);
        count = 0;
        for(int i = 0 ;i<selectedRow.length;i++){
            Product p = (Product)productCatalog.getValueAt(selectedRow[i],0);
            Product product1 = salesperson.getProductCatalog().addProduct();
            product1.setProdName(p.getProdName());
            product1.setFloorprice(p.getFloorprice());
            product1.setCeilingprice(p.getCeilingprice());
            product1.setTargetprice(p.getTargetprice());
            product1.setAvail(p.getAvail());
            
            count++;
        }
                
        if(count > 0)
        {
            JOptionPane.showMessageDialog(this, "Drugs successfully added", "Information",JOptionPane.INFORMATION_MESSAGE);
        }
        else
        
        JOptionPane.showMessageDialog(this, "Kindly select the drugs need to be added", "Information",JOptionPane.INFORMATION_MESSAGE);  
    }//GEN-LAST:event_addProductButtonActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
    userProcessContainer.remove(this);
    CardLayout layout =(CardLayout)userProcessContainer.getLayout();
    layout.previous(userProcessContainer);


    }//GEN-LAST:event_backButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addProductButton;
    private javax.swing.JButton backButton;
    private javax.swing.JTextField commisionField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable productCatalog;
    // End of variables declaration//GEN-END:variables
}