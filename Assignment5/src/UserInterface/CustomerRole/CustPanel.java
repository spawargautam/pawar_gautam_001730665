/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.CustomerRole;

import Business.Customer;
import Business.MasterOrderCatalog;
import Business.ProductCatalog;
import Business.customerDirectory;
import Business.salesPerson;
import Business.salespersonDirectory;
import java.awt.CardLayout;
import javax.swing.JPanel;


public class CustPanel extends javax.swing.JPanel {
    private JPanel userProcessContainer;
    private customerDirectory customerdirectory;
    private ProductCatalog productcatalog;
    private salesPerson salesperson; 
    private salespersonDirectory salesdirectory;
    private MasterOrderCatalog masterordercatalog;
    private Customer customer;
    
    public CustPanel(JPanel upc,ProductCatalog pc,salespersonDirectory sd,salesPerson sp,customerDirectory cd,MasterOrderCatalog moc,Customer customer) {
        initComponents();
        this.userProcessContainer=upc;
        this.salesdirectory=sd;
        this.customerdirectory=cd;
        this.salesperson=sp;        
        this.customerdirectory=cd;
        this.masterordercatalog=moc;
        this.customer=customer;
        populateSalesCombo();      
        
    }

    
    private void populateSalesCombo(){
        customerComboBox.removeAllItems();;
        for (Customer customer1 : customerdirectory.getCustList()) 
            customerComboBox.addItem(customer1);
    }
    
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        customerComboBox = new javax.swing.JComboBox();
        buttonGO = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        jLabel1.setText("Customers ");

        customerComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        customerComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customerComboBoxActionPerformed(evt);
            }
        });

        buttonGO.setText("GO>>");
        buttonGO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonGOActionPerformed(evt);
            }
        });

        jLabel2.setText("Customer Name");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(278, 278, 278)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(133, 133, 133)
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(customerComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(buttonGO)))
                .addContainerGap(198, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jLabel1)
                .addGap(64, 64, 64)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(customerComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonGO, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addContainerGap(318, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void customerComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customerComboBoxActionPerformed
         
    }//GEN-LAST:event_customerComboBoxActionPerformed

    private void buttonGOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonGOActionPerformed
        Customer customer1 = (Customer)customerComboBox.getSelectedItem();
        Customer customer = (Customer) customerComboBox.getSelectedItem();
        BrowseProducts bp = new BrowseProducts(userProcessContainer,productcatalog,salesdirectory,salesperson,masterordercatalog,customer1);
        userProcessContainer.add("SupplierWorkAreaJPanel", bp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_buttonGOActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonGO;
    private javax.swing.JComboBox customerComboBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables
}
