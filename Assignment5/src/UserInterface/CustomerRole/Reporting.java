/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.CustomerRole;

import Business.Customer;
import Business.OrderItem;
import Business.customerDirectory;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;


public class Reporting extends javax.swing.JPanel {
    private JPanel userProcessContainer;
  //  private customerDirectory cd;
    private Customer customer;
    public Reporting(JPanel userProcessContainer,Customer c) {
        initComponents();
        this.userProcessContainer=userProcessContainer;
        this.customer=c;
        refreshTable();
    }

    public void refreshTable()
    {
        int rowCount = customerTable.getRowCount();
        DefaultTableModel model = (DefaultTableModel) customerTable.getModel();
        for(int i=rowCount - 1;i>=0;i--)
        {
            model.removeRow(i);
        }
       for (OrderItem o:customer.getOrder().getOrderItemList())
       {
            Object row[] = new Object[3];
            row[0] = o;
            row[1] = o.getQuantity();
            row[2] = o.getSalesPrice();
            model.addRow(row);
       }
    }
    
    
    
    
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        customerTable = new javax.swing.JTable();

        customerTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Ab", "aa", "sa"
            }
        ));
        jScrollPane1.setViewportView(customerTable);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(150, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 361, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(214, 214, 214))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(12, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(243, 243, 243))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable customerTable;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
