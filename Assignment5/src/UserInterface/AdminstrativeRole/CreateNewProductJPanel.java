package UserInterface.AdminstrativeRole;

import Business.Product;
import Business.ProductCatalog;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


/**
 *
 * @author Mihir Mehta / Hechen Gao
 */
public class CreateNewProductJPanel extends javax.swing.JPanel {

   private  JPanel userProcessContainer;
   private ProductCatalog productcatalog;
    public CreateNewProductJPanel(JPanel upc,ProductCatalog pc){
        initComponents();
        this.userProcessContainer = upc;
        this.productcatalog=pc;
        
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        floorPricefield = new javax.swing.JTextField();
        btnAdd = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        availField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        celingPriceField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        targetPricefield1 = new javax.swing.JTextField();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("Create New Product");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 20, -1, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Availability");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 300, 110, 30));

        floorPricefield.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        add(floorPricefield, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 140, 160, 30));

        btnAdd.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnAdd.setText("Add Product");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        add(btnAdd, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 360, -1, -1));

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 350, -1, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setText("Product Name:");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 90, -1, 30));

        txtName.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        add(txtName, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 90, 210, -1));

        availField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                availFieldActionPerformed(evt);
            }
        });
        add(availField, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 300, 170, 30));

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setText("Floor Price:");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 140, 110, 30));

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel7.setText("Ceiling Price:");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 200, 110, 30));

        celingPriceField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                celingPriceFieldActionPerformed(evt);
            }
        });
        add(celingPriceField, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 200, 170, 30));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setText("Target Price:");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 240, 110, 30));

        targetPricefield1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                targetPricefield1ActionPerformed(evt);
            }
        });
        add(targetPricefield1, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 240, 170, 30));
    }// </editor-fold>//GEN-END:initComponents
    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed

        Product p =productcatalog.addProduct();
        System.out.print("abc");
        p.setProdName(txtName.getText());
        p.setFloorprice(Integer.parseInt(floorPricefield.getText()));
        p.setCeilingprice(Integer.parseInt(celingPriceField.getText()));
        p.setTargetprice(Integer.parseInt(targetPricefield1.getText()));
        p.setAvail(Integer.parseInt(availField.getText()));
        JOptionPane.showMessageDialog(null, "Product added!", "Info", JOptionPane.INFORMATION_MESSAGE);
}//GEN-LAST:event_btnAddActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed

        userProcessContainer.remove(this);
        Component[] componentArray = userProcessContainer.getComponents();
        Component component = componentArray[componentArray.length - 1];
        ManageProductCatalogJPanel manageProductCatalogJPanel = (ManageProductCatalogJPanel) component;
        manageProductCatalogJPanel.refreshTable();
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void availFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_availFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_availFieldActionPerformed

    private void celingPriceFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_celingPriceFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_celingPriceFieldActionPerformed

    private void targetPricefield1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_targetPricefield1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_targetPricefield1ActionPerformed
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField availField;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnBack;
    private javax.swing.JTextField celingPriceField;
    private javax.swing.JTextField floorPricefield;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JTextField targetPricefield1;
    private javax.swing.JTextField txtName;
    // End of variables declaration//GEN-END:variables
}
