/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class Product {
    
    private String prodName;
    private int floorprice;
    private int ceilingprice;
    private int targetprice;
    private int modelNumber;
    private int avail;
    private static int count =0;
    private int soldQuantity;
    private int salesprice;

    public int getSalesprice() {
        return salesprice;
    }

    public void setSalesprice(int salesprice) {
        this.salesprice = salesprice;
    }
    
   

    public int getSoldQuantity() {
        return soldQuantity;
    }

    public void setSoldQuantity(int soldQuantity) {
        this.soldQuantity = soldQuantity;
    }

    

    public int getFloorprice() {
        return floorprice;
    }

    public void setFloorprice(int floorprice) {
        this.floorprice = floorprice;
    }

    public int getCeilingprice() {
        return ceilingprice;
    }

    public void setCeilingprice(int ceilingprice) {
        this.ceilingprice = ceilingprice;
    }

    public int getTargetprice() {
        return targetprice;
    }

    public void setTargetprice(int targetprice) {
        this.targetprice = targetprice;
    }

    @Override
    public String toString() {
        return prodName; //To change body of generated methods, choose Tools | Templates.
    }

    
    public Product() {
    count++;
    modelNumber = count;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }


    public int getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(int modelNumber) {
        this.modelNumber = modelNumber;
    }

    public int getAvail() {
        return avail;
    }

    public void setAvail(int avail) {
        this.avail = avail;
    }
    
    
    
}
