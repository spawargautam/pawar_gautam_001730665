/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author new
 */
public class Business {
    private salespersonDirectory salesDirectory;
    private MasterOrderCatalog masterOrderCatalog;

    public salespersonDirectory getSalesDirectory() {
        return salesDirectory;
    }

    public void setSalesDirectory(salespersonDirectory salesDirectory) {
        this.salesDirectory = salesDirectory;
    }

   

    public MasterOrderCatalog getMasterOrderCatalog() {
        return masterOrderCatalog;
    }

    public void setMasterOrderCatalog(MasterOrderCatalog masterOrderCatalog) {
        this.masterOrderCatalog = masterOrderCatalog;
    }

    public Business() {
        salesDirectory= new salespersonDirectory();
        masterOrderCatalog= new MasterOrderCatalog();
    }
    
}
