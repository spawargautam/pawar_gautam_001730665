/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author new
 */
public class customerDirectory {
    
    
    private ArrayList<Customer> custList;
    
    public customerDirectory()
    {
        custList = new ArrayList<Customer>();
    }

    public ArrayList<Customer> getCustList() {
        return custList;
    }

    public void setCustList(ArrayList<Customer> custList) {
        this.custList = custList;
    }
    
   public Customer addCustomer()
    {
        Customer c = new Customer();
        custList.add(c);
        return c;
    }
    
   public void removeCustomer(Customer c)
       
   {
       custList.remove(c);
   }
   
   
   
}
