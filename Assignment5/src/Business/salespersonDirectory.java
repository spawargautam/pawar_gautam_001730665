/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author new
 */
public class salespersonDirectory {
    
    private ArrayList<salesPerson> salepersonList;

    public ArrayList<salesPerson> getSalepersonList() {
        return salepersonList;
    }

    public void setSalepersonList(ArrayList<salesPerson> salepersonList) {
        this.salepersonList = salepersonList;
    }
    
    public salespersonDirectory()
    {
        salepersonList = new ArrayList<salesPerson>();
    }
    
    
    public salesPerson addsalesPerson()
    {
        salesPerson sp = new salesPerson();
        salepersonList.add(sp);
        return sp;
    }
    
    public void removesalesPerson(salesPerson s)
    {
        salepersonList.remove(s);
    }
    
}
