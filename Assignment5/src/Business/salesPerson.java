/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author new
 */
public class salesPerson {
    
    
    private String name;
    private int employeeId;
    private ProductCatalog productCatalog;
    private int commision;
    private int prodAboveTarget;
    private int prodBelowTarget;

    public int getProdAboveTarget() {
        return prodAboveTarget;
    }

    public void setProdAboveTarget(int prodAboveTarget) {
        this.prodAboveTarget = prodAboveTarget;
    }

    public int getProdBelowTarget() {
        return prodBelowTarget;
    }

    public void setProdBelowTarget(int prodBelowTarget) {
        this.prodBelowTarget = prodBelowTarget;
    }

    public int getCommision() {
        return commision;
    }

    public void setCommision(int commision) {
        this.commision = commision;
    }

    public ProductCatalog getProductCatalog() {
        return productCatalog;
    }

    public void setProductCatalog(ProductCatalog productCatalog) {
        this.productCatalog = productCatalog;
    }


    public salesPerson()
    {
            productCatalog = new ProductCatalog();

    }
    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public String toString() {
        return name;
    }
    
    
}
